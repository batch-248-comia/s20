console.log("Hello, B248!");

/* Mini Activity
Create a function named greeting and display a message you want to say to yourself using console.log() inside of the function. Invoke the greeting function.

*/

function greeting(){
	console.log("You can do this!");
}

greeting();

// We can just use our loops

let countNum = 20;

while(countNum !==0){
	console.log("This is printed inside the loop" + countNum);
	greeting();
	countNum--;
}

//[While Loop]

/*
	a while loop takes in an expression/condition
	-expressions are any unit of code that can be evaluated to a value
	-if the condition evaluates to tru, the statement inside the code block will be executed
	- a statement is a command that the programmer gives to a computer

	Loop will iterate a certain number of times until an expression/condition is met

	- iteration is the term given to the repetition of statements

	Syntax

	while(expression/condition){
		statement
	}
*/
//While the count is not equal to zero
		let count = 5; //if we put 0, it will not run, it means false

		while(count !==0){
			//the current value of the count is also printed out
			console.log("While: " + count);

			//this decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
			
			//after running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console QUICKLY CLOSE the application/browser/tab to avoid this

			count--;//DO NOT FORGET
		}

//[DO-WHILE LOOP]

/* 

		A do while loop works a lot like while loop, but unlike while loops, do-while loops guarantee that the code will be executed at least once

		Syntax

			do {
				statement
			} while (expression/condition)

*/

		let number = Number(prompt("Give me a number!"));

		do {
			console.log("Do while: " + number);
			//increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
			//number = number + 1
			number += 1;

		//providing a number of 10 or greater will run the code block ONCE adn will STOP the loop
		}while(number < 10)


//[FOR LOOP]

/*
	A for loop is more flexible that while and do-while loops. It consists of three parts:
	1. initialization - will track the progression
	2. expression/condition
	3. finalExpression

*/

		for (let count = 0; count <=20; count++){
			console.log("For Loop: " + count);
		}

/*

refactor the code above that will only print the even numbers

*/

		for (let count = 0; count <=20; count++){
			if(count % 2 == 0){
				console.log("Even: " + count);
			}
		}

//example

			let myString = "Vice Ganda"
			console.log(myString.length);//10 -- count space

			console.log(myString[0]);//V
			console.log(myString[1]);//i
			console.log(myString[2]);//c
			console.log(myString[9]);


			for(let x = 0; x<myString.length;x++){
				console.log(myString[x])
			}

//example

			let myName = "Zeref Dragneel";

			for (let i=0; i < myName.length; i++){
				if(
					myName[i].toLowerCase() == "a" || 
					myName[i].toLowerCase() == "e" || 
					myName[i].toLowerCase() == "i" || 
					myName[i].toLowerCase() == "o" || 
					myName[i].toLowerCase() == "u" 
					){

					console.log("Hi I'm a vowel!")

				}else{
					console.log(myName[i])
				}
			}

//Continue and Break Statements
/*
	The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

	The break statement is used to terminate the current loop once a match has been found
*/

for(let count =0; count <=20; count++){

	if(count % 2 ===0){
		//tells the code to continue to next iteration of the loop
		//this ignores all statements located after the continue statement
		continue;
	}

	console.log("Continue and Break: " + count);

	if(count>10){
		//this the tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute as long as the value of count is less than or equal to 20
		// the other number values will no longer be printed
		break;
	}
}

let name = "Mommy Dragneel Yonisia"

for(let i = 0; i < name.length; i++){
	console.log(name[i])

	//if the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase()==="a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] =="Y"){
		break;
	}
}